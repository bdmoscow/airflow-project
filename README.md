В этом проекте содержится практическая работа, выполненная в рамках изучения Airflow. В рамках выполнения работы был построен
полный цикл разведки, обработки пропусков и аномалий, нормализации данных, кодирования категориальных переменных и обучения моделей
предсказания ценовой категории автомобиля: low, medium, high.

Скрипты пайплайна (обработки тренировочных данных и обучения модели) и предикта модели находятся в папке modules/

Для работы по расписанию написан скрипт узла графа DAG.

Новые модели сохраняются в папке data/models в формате .pkl с уникальным именем формата даты и времени. Тестовые данные
подаются из папки data/test в формате .json, а предикты по всем тестам сохраняются в папке data/predictions в формате .csv
с указанием id модели автомобиля и категорией.

Последние тесты показали, что наилучшая обученная модель - SVC с accurcay=0.7871

Для запуска Airflow весь проект упакован в Docker контейнер, ямл инициализации сохранен в файле docker-compose.yaml
